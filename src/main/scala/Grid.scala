case class Grid(val x_max: Int, val y_max: Int) {
  override def toString: String =
  s"x_max=$x_max, y_max=$y_max"

}