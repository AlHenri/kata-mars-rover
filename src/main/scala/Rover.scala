import scala.math._

class Rover(val x: Int, val y: Int, val orientation: Char, val grid: Grid=Grid(5,5)) {
  private val card_points = Seq('N', 'E', 'S', 'W')

  private def get_next_coordinates(new_val: Int, is_x: Boolean) = {
    if (is_x) {
      max(min(new_val, grid.x_max), 0)
    } 
    else {
      max(min(new_val, grid.y_max), 0)
    }
  }

  def move()={
    orientation match {
      case 'N' => new Rover(x, get_next_coordinates(y+1, false), orientation)
      case 'S' => new Rover(x, get_next_coordinates(y-1, false), orientation)
      case 'W' => new Rover(get_next_coordinates(x-1, true), y, orientation)
      case 'E' => new Rover(get_next_coordinates(x+1, true), y, orientation)
    }
  }

  def execute_command(action: Char) = action match {
      case 'L' => new Rover(x=x, y=y, orientation=card_points((card_points.indexOf(orientation) + 3) % 4))
      case 'R' => new Rover(x=x, y=y, orientation=card_points((card_points.indexOf(orientation) + 1) % 4))
      case 'M' => this.move()
      case _  => new Rover(x=x, y=y, orientation=orientation)
  }

  def execute_list_command(actions: String) = {
    val actionList = actions.split("")
    actionList.foldLeft(this){
      (rover, action) => rover.execute_command(action.charAt(0))
    }
  }

  override def toString: String =
  s"Position=($x,$y) Orientation=$orientation"
}