class MainTest extends org.scalatest.funsuite.AnyFunSuite {
  test("Test process input") {
    val input= """
      5 5
      1 2 N
      LMLMLMLMM
      3 3 E
      MMRMMRMRRM
    """.stripMargin

    val main = new Main()
    
    assert(main.process_input(input) == "Position=(1,3) Orientation=N\nPosition=(5,1) Orientation=E")
  }
}