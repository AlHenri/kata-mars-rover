class RoverTest extends org.scalatest.funsuite.AnyFunSuite {
  test("Should check rover position") {
    val rover = new Rover(x=2, y=2, orientation='N')
    assert(rover.toString == "Position=(2,2) Orientation=N")
  }

  test("Should turn right") {
    val action = 'R'
    val rover = new Rover(x=2, y=2, orientation='N')

    val end_rover = rover.execute_command(action)

    assert(end_rover.toString == "Position=(2,2) Orientation=E")
  }

  test("Should turn left") {
    val action = 'L'
    val rover = new Rover(x=2, y=2, orientation='N')

    val end_rover = rover.execute_command(action)

    assert(end_rover.toString == "Position=(2,2) Orientation=W")
  }

  test("Should move") {
    val action = 'M'
    val rover = new Rover(x=2, y=2, orientation='N')
    val end_rover = rover.execute_command(action)
    assert(end_rover.toString == "Position=(2,3) Orientation=N")
  }

  test("Test if Rover position is valid") {
    val grid = new Grid(3, 3)
    val rover = new Rover(x=3, y=3, orientation='N', grid=grid)
    assert(rover.x <= grid.x_max)
    assert(rover.y <= grid.y_max)
  }

  test("Should not move to stay inside the grid") {
    val action = 'M'
    val grid = new Grid(3, 3)
    val rover = new Rover(x=3, y=3, orientation='N', grid=grid)
    val end_rover = rover.execute_command(action)
    assert(end_rover.toString == "Position=(3,3) Orientation=N")
  }

  test("Should move multiple time rover 1") {
    val actions = "LMLMLMLMM"
    val grid = new Grid(5, 5)
    val rover = new Rover(x=1, y=2, orientation='N', grid=grid)
    val end_rover = rover.execute_list_command(actions)
    assert(end_rover.toString == "Position=(1,3) Orientation=N")
  }

  test("Should move multiple time rover 2") {
    val actions = "MMRMMRMRRM"
    val grid = new Grid(5, 5)
    val rover = new Rover(x=3, y=3, orientation='E', grid=grid)
    val end_rover = rover.execute_list_command(actions)
    assert(end_rover.toString == "Position=(5,1) Orientation=E")
  }

  test("Should move multiple time rover 2 with wall") {
    val actions = "MMMMMMMMRMMRMRRM"
    val grid = new Grid(5, 5)
    val rover = new Rover(x=3, y=3, orientation='E', grid=grid)
    val end_rover = rover.execute_list_command(actions)
    assert(end_rover.toString == "Position=(5,1) Orientation=E")
  }

  // test("Test if Rover position is valid") {
  //   val grid = new Grid(3, 3)
  //   val rover = new Rover(x=5, y=3, orientation='N', grid=grid)
  //   assert(rover.x <= grid.x_max)
  //   assert(rover.y <= grid.y_max)
  // }
}