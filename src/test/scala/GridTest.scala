class GridTest extends org.scalatest.funsuite.AnyFunSuite {
  test("PrintGridDimension") {
    val grid = new Grid(x_max=5, y_max=5)
    assert(grid.toString == "x_max=5, y_max=5")
  }

}